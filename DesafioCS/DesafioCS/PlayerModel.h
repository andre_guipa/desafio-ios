//
//  PlayerModel.h
//  DesafioCS
//
//  Created by André Menezes on 30/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import "MTLModel.h"
#import <MTLJSONAdapter.h>

@interface PlayerModel : MTLModel<MTLJSONSerializing>
@property (nonatomic, strong) NSString* playerName;
@property (nonatomic, strong) NSString* playerAvatarUrl;
@property (nonatomic, strong) NSString* playerUserName;


@end
