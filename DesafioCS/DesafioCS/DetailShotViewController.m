//
//  DetailShotViewController.m
//  DesafioCS
//
//  Created by André Menezes on 30/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import "DetailShotViewController.h"

@interface DetailShotViewController (){
            UIActivityIndicatorView     *activityIndicator;
}

@end

@implementation DetailShotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController].navigationBar setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.imgViewPlayerAvatar.layer.cornerRadius = 30.0;
    self.imgViewPlayerAvatar.layer.masksToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    [self configShot];
    
}

#pragma mark - Methods

-(void)configShot{
    self.labelPlayerName.text = self.shot.player.playerName;
    
    NSString *descriptionText = [NSString stringWithFormat:@"<b>%@</b>",self.shot.title];
    
    
    if (self.shot.desc.length > 0) {
        descriptionText = [descriptionText stringByAppendingFormat:@": %@",self.shot.desc];
    }
    
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[descriptionText dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSForegroundColorAttributeName : [UIColor whiteColor] } documentAttributes:nil error:nil];
    
    
    self.textViewDesc.attributedText = attrStr;
    
    NSString *url;
    url =  self.shot.image400Url;
    if (!self.shot.image400Url) {
        url = self.shot.imageUrl;
    }
    
    [self configImageView:self.imgViewShot withUrl:url];
    [self configImageView:self.imgViewPlayerAvatar withUrl:self.shot.player.playerAvatarUrl];
}

-(void)configImageView:(UIImageView *)imageView withUrl:(NSString *)shotUrl{
    
    activityIndicator = [UIActivityIndicatorView new];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray   ];
    [activityIndicator setCenter:CGPointMake(CGRectGetWidth(imageView.frame) / 2, CGRectGetHeight(imageView.frame) / 2)];
    [imageView addSubview:activityIndicator];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];

    [self setImageWithActiveIndicatorWithImageView:imageView andURL:shotUrl];
    
}


-(void)setImageWithActiveIndicatorWithImageView:(UIImageView *)mainImage andURL:(NSString *)urlString{
    
    [mainImage setImage:nil];
    [activityIndicator setHidden:NO];
    [activityIndicator startAnimating];
    
    __weak typeof(activityIndicator) weakActivityIndicator = activityIndicator;
    __weak typeof(mainImage) weakCardImage = mainImage;
    [mainImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]
                     placeholderImage:nil
                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                  [weakActivityIndicator setHidden:YES];
                                  [weakActivityIndicator stopAnimating];
                                  [weakCardImage setImage:image];
                                  [weakCardImage setImage:image];
                              }
                              failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                  [weakActivityIndicator setHidden:YES];
                                  [weakActivityIndicator stopAnimating];
                              }];
    
}

#pragma mark - IBAction's

- (IBAction)actShareOnFacebook:(UIButton *)sender {

    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController * fbSheetOBJ = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [fbSheetOBJ addImage:self.imgViewShot.image];
        
        [self presentViewController:fbSheetOBJ animated:YES completion:Nil];
    }
}

- (IBAction)actShareOnTwitter:(UIButton *)sender {
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [controller addImage:self.imgViewShot.image];
    [self presentViewController:controller animated:YES completion:Nil];
}



@end
