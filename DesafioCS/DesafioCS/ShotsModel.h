//
//  ShotsModel.h
//  DesafioCS
//
//  Created by André Menezes on 28/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import "MTLModel.h"
#import <MTLJSONAdapter.h>
#import "PlayerModel.h"


@interface ShotsModel : MTLModel<MTLJSONSerializing>
@property (nonatomic, strong) NSNumber* shotID;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* desc;
@property (nonatomic, strong) NSNumber* viewsCount;
@property (nonatomic, strong) NSString* imageUrl;
@property (nonatomic, strong) NSString* imageTeaserUrl;
@property (nonatomic, strong) NSString* image400Url;
@property (nonatomic, strong) PlayerModel* player;
@end

