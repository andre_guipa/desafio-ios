//
//  DetailShotViewController.h
//  DesafioCS
//
//  Created by André Menezes on 30/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShotsModel.h"
#import "UIImageView+AFNetworking.h"
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>

@interface DetailShotViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgViewShot;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPlayerAvatar;
@property (strong, nonatomic) ShotsModel *shot;
@property (weak, nonatomic) IBOutlet UILabel *labelPlayerName;
@property (weak, nonatomic) IBOutlet UITextView *textViewDesc;
@property (weak, nonatomic) IBOutlet UIButton *btShareonTwitter;

@end
