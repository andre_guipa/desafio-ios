//
//  ViewController.h
//  DesafioCS
//
//  Created by André Menezes on 30/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "MTLJSONAdapter.h"
#import "HomeCollectionViewCell.h"
#import "DetailShotViewController.h"
#import "UIScrollView+DXRefresh.h"
#import <SVProgressHUD.h>

@interface HomeViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionVIewHome;
@property (strong, nonatomic) NSMutableArray *arrayOfShots;
@property (assign, nonatomic) NSInteger page;

@end

