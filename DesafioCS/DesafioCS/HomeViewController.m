//
//  ViewController.m
//  DesafioCS
//
//  Created by André Menezes on 30/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import "HomeViewController.h"


@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    self.arrayOfShots = [[NSMutableArray alloc] init];
    self.page = 1;
    [self.collectionVIewHome addFooterWithTarget:self action:@selector(pagingShots) withIndicatorColor:[UIColor whiteColor]];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self pagingShots];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self navigationController].navigationBar setHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

-(void)pagingShots{
    
    if (self.page == 1) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    }
    
    [[WebService sharedInstance] getHomeShotsWithPageNumber:self.page and:^(id responseObject) {
        [SVProgressHUD dismiss];
       [self setupShotsArrayWithDictionary:[responseObject objectForKey:@"shots"]];
       [self.collectionVIewHome reloadData];
       [self.collectionVIewHome footerEndRefreshing];
       self.page++;
    } failure:^(NSString *errorMessage) {
        [SVProgressHUD dismiss];
    }];
    
}

-(void)setupShotsArrayWithDictionary:(NSArray *)arrayShots{
    
    for (NSInteger index = 0; index < arrayShots.count; index ++) {
        NSError *error = nil;
        ShotsModel *shot = [MTLJSONAdapter modelOfClass:[ShotsModel class] fromJSONDictionary:[arrayShots objectAtIndex:index] error:&error];
        [self.arrayOfShots addObject:shot];
        
    }
    self.collectionVIewHome.delegate = self;
    self.collectionVIewHome.dataSource = self;
}

#pragma mark - UICollectionViewDelegate/DataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.arrayOfShots.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"HomeCell";
    
    HomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

    ShotsModel *shot = [self.arrayOfShots objectAtIndex:indexPath.row];
    
    [cell setCellWithShot:shot];
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"segueShotDetail" sender:[self.arrayOfShots objectAtIndex:indexPath.row]];
}

#pragma mark - Navigation Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"segueShotDetail"]) {
        DetailShotViewController *detailViewController = [segue destinationViewController];
        detailViewController.shot = (ShotsModel *)sender;
    }
    
}

#pragma mark - StatusBarStyle

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
