//
//  WebService.h
//  DesafioCS
//
//  Created by André Menezes on 28/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShotsModel.h"
#import <MTLJSONAdapter.h>

@interface WebService : NSObject
+ (WebService *)sharedInstance;
- (void)getHomeShotsWithPageNumber:(NSInteger)pageNumber and:(void (^)(id responseObject))success failure:(void (^)(NSString *errorMessage))failure;

@end
