//
//  HomeCollectionViewCell.h
//  DesafioCS
//
//  Created by André Menezes on 30/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShotsModel.h"
#import "UIImageView+AFNetworking.h"

@interface HomeCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewShot;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelViewCount;

-(void)setCellWithShot:(ShotsModel *)shot;

@end
