//
//  ShotsModel.m
//  DesafioCS
//
//  Created by André Menezes on 28/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import "ShotsModel.h"

@implementation ShotsModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"shotID": @"id",
             @"title": @"title",
             @"desc": @"description",
             @"viewsCount": @"views_count",
             @"imageUrl": @"image_url",
             @"imageTeaserUrl": @"image_teaser_url",
             @"image400Url": @"image_400_url",
             @"player": @"player"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    NSError *serError = nil;
    
    self.player = [MTLJSONAdapter modelOfClass:[PlayerModel class] fromJSONDictionary:[dictionaryValue objectForKey:@"player"] error:&serError];
    
    return self;
}

@end
