//
//  HomeCollectionViewCell.m
//  DesafioCS
//
//  Created by André Menezes on 30/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import "HomeCollectionViewCell.h"

@implementation HomeCollectionViewCell{
        UIActivityIndicatorView     *activityIndicator;
    
}

-(void)setCellWithShot:(ShotsModel *)shot{
    
    self.labelTitle.text = shot.title;
    self.labelViewCount.text = [shot.viewsCount stringValue];
    
    [self configShotImageWithUrl:shot.imageTeaserUrl];
}

-(void)configShotImageWithUrl:(NSString *)shotUrl{
    if (activityIndicator) {
        [activityIndicator removeFromSuperview];
    }
    activityIndicator = [UIActivityIndicatorView new];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [self.imgViewShot addSubview:activityIndicator];
    [self.imgViewShot setContentMode:UIViewContentModeScaleAspectFill];
    [activityIndicator setCenter:CGPointMake(CGRectGetWidth(self.imgViewShot.frame) / 2, CGRectGetHeight(self.imgViewShot.frame) / 2)];
    [self setImageWithActiveIndicatorWithImageView:self.imgViewShot andURL:shotUrl];
    
}


-(void)setImageWithActiveIndicatorWithImageView:(UIImageView *)mainImage andURL:(NSString *)urlString{
    [mainImage setImage:nil];
    [activityIndicator setHidden:NO];
    [activityIndicator startAnimating];
    
    __weak typeof(activityIndicator) weakActivityIndicator = activityIndicator;
    __weak typeof(mainImage) weakCardImage = mainImage;
    [mainImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]
                     placeholderImage:nil
                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                  [weakActivityIndicator setHidden:YES];
                                  [weakActivityIndicator stopAnimating];
                                  [weakCardImage setImage:image];
                              }
                              failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                  [weakActivityIndicator setHidden:YES];
                                  [weakActivityIndicator stopAnimating];
                              }];
    
}

@end
