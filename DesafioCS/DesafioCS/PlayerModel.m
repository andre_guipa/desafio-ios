//
//  PlayerModel.m
//  DesafioCS
//
//  Created by André Menezes on 30/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import "PlayerModel.h"

@implementation PlayerModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"playerName": @"name",
             @"playerAvatarUrl": @"avatar_url",
             @"playerUserName": @"username"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    
    return self;
}

@end
