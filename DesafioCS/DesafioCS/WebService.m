//
//  WebService.m
//  DesafioCS
//
//  Created by André Menezes on 28/04/15.
//  Copyright (c) 2015 Andre Menezes. All rights reserved.
//

#import "WebService.h"
#import <SVProgressHUD.h>
#import <AFNetworking/AFNetworking.h>



#define MAIN_URL_SERVICE        @"http://api.dribbble.com/shots/popular?page="
#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

@implementation WebService

+ (WebService *)sharedInstance {
    static WebService *sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)getHomeShotsWithPageNumber:(NSInteger)pageNumber and:(void (^)(id responseObject))success failure:(void (^)(NSString *errorMessage))failure {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%li",MAIN_URL_SERVICE,(long)pageNumber];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:10.0];
    [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success([self createJSONWithData:responseObject]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    [operation start];
}

- (id)createJSONWithData:(NSData *)data {
    __weak id jsonObject;
    NSError *parseError;
    
    if (data) {
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&parseError];
    } else {

    }
    return NULL_TO_NIL(jsonObject);
}


@end
